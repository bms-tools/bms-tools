#!/usr/bin/env python
import bmstools.jbd
import serial
import sys
import time
from pprint import pprint
import json
import argparse


class Test:

    def __init__(self, port = '/dev/ttyUSB0'):
        s = serial.Serial(port)
        self.j = bmstools.jbd.JBD(s)
        self.j.debug=True

    def test(self):
        self.j.debug=True
        self.j.password = 'abcdef'

        self.j.open()
        for _ in range(3):
            try:
                j._sendPassword()
                print('send password success')
            except Exception as e:
                print(f'attempt {_} error {repr(e)}')

    def clearErrors(self):
        self.j.clearErrors()

    def main(self):

        if 0:
            self.j.clearPasswordNoFactory()
            return

        if 0:
            self.j.clearPassword()
            return

        if 0:
            self.j.setPassword('abcdef')
            return


        if 0:
            self.j.password = 'xxxxxx'
            #self.j.password = bytes(6)
            self.clearErrors()
            return

        if 0:
            print(repr(serial_num_reg))
            print(serial_num_reg.get('serial_num'))
            print(f"serial number: {serial_num_reg.get(serial_num_reg.regName)}")

            reg = j.eeprom_reg_by_regname['error_cnts']
            reg = j.readReg(reg)
            for k,v in reg.items():
                print(k,v)

        while 1:
            time.sleep(1)
            try:
                basic = self.j.readBasicInfo()
                cell = self.j.readCellInfo()
                print(json.dumps(basic, indent = 2))
                print(json.dumps(cell, indent = 2))
            except:
                pass

def getArgs():
    p = argparse.ArgumentParser()
    p.add_argument('-p', '--port', default = '/dev/ttyUSB0')
    return p.parse_args()

def main():
    args = getArgs()
    test = Test(args.port)
    test.main()

main()
